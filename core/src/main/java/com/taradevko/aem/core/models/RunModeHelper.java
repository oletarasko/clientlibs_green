package com.taradevko.aem.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.settings.SlingSettingsService;

@Model(adaptables=Resource.class)
public class RunModeHelper {

    @Inject
    private SlingSettingsService settings;

    public boolean isAuthor() {
        return settings.getRunModes().contains("author");
    }
}
